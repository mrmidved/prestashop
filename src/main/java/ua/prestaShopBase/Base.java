package ua.prestaShopBase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.beans.EventHandler;

public class Base extends Property {

    protected WebDriver driver;
    protected EventFiringWebDriver efwd;

    protected Base(WebDriver driver){
        super();
        this.driver = driver;
        this.efwd = new EventFiringWebDriver(this.driver);
        this.efwd.register(new LogerEventHandler());
    }
}

