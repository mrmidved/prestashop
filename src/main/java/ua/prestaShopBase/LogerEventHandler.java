package ua.prestaShopBase;

import org.apache.log4j.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;


public class LogerEventHandler implements WebDriverEventListener {
    private static final Logger LOG = LogManager.getLogger(LogerEventHandler.class);

    @Override
    public void beforeAlertAccept(WebDriver webDriver) {
        LOG.info("1");
    }

    @Override
    public void afterAlertAccept(WebDriver webDriver) {
        LOG.info("2");
    }

    @Override
    public void afterAlertDismiss(WebDriver webDriver) {
        LOG.info("3");
    }

    @Override
    public void beforeAlertDismiss(WebDriver webDriver) {
        LOG.info("4");
    }

    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        LOG.info("WebDriver navigated from '" + s + "'");
    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {
        LOG.info("WebDriver navigated to '" + s + "'");
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {
        LOG.info("7");
    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {
        LOG.info("8");
    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {
        LOG.info("9");
    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        LOG.info("10");
    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {
        LOG.info("11");
    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {
        LOG.info("12");
    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        LOG.info("Should be " + by);
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        LOG.info("Element found");
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        LOG.info("Should click " + webElement.getTagName());
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        LOG.info("Clicked successfull");
    }


    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        LOG.info("13");
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        LOG.info("14");
    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        LOG.info("15");
    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        LOG.info("16");
    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {
        LOG.info("17");
    }
}
