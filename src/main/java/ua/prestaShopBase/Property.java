package ua.prestaShopBase;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class Property {
    protected Properties property;

    public Property(){
        this.GetProperty();
    }

    private void GetProperty() {
        FileInputStream fis;
        this.property = new Properties();
        try {
            fis = new FileInputStream("src/main/resources/config/config.properties");
            //property.load(fis);
            property.load( new InputStreamReader(fis,"UTF-8"));
        } catch (IOException e) {
            System.err.println("ERROR: The properties file is missing!");
        }
    }
}
