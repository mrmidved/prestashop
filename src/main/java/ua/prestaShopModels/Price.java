package ua.prestaShopModels;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Price {
    private double price;
    private double discount;
    private double priceDiscount;

    public Price(){}

    public static Double GetStringForDouble(String str)
    {
        return Double.parseDouble(str.substring(0,str.length()-1)
                .trim().replaceAll(",","."));
    }

    public void SetPrice(String price,String discount,String priceDiscount )
    {
        this.price = Price.GetStringForDouble(price);
        this.discount = Double.parseDouble(discount.replaceAll("\\D",""))/100;
        this.priceDiscount = Price.GetStringForDouble(priceDiscount);
    }

    public boolean IsCorrectDiscount()
    {
        DecimalFormat df = new DecimalFormat("#.##");
        double calcDiscount =Double.parseDouble(
               df.format(this.price - (this.price*this.discount)).replaceAll(",","."));
        return this.priceDiscount == calcDiscount;
    }
}
