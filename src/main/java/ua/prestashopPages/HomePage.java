package ua.prestashopPages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ua.prestaShopBase.Base;
import ua.prestaShopBase.LogerEventHandler;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class HomePage extends Base {

    public HomePage(WebDriver driver){
        super(driver);
        this.efwd.get(this.property.getProperty("db.Url"));
    }
    // private static Logger log = Logger.getLogger(HomePage.class.getName());

    // the method checks the currency in the goods in the field of popular goods
    public boolean IsCurrencyPriceCheck() {
        WebElement сurrency = driver.findElement(By.cssSelector(
                "[class=\"expand-more _gray-darker hidden-sm-down\"]"));
        String curText = сurrency.getText();
        char curSymbol = curText.charAt(curText.length()-1);

       List<WebElement> productsInPage = driver.findElements(By.xpath(
               ".//div[@class='products']/article/div/div[@class='product-description']/div/span[@itemprop='price']"));

        for(WebElement el :productsInPage){
            String curTextProduct = el.getText();
            char curSymbolProuct = curTextProduct.charAt(curTextProduct.length()-1);
            if(curSymbolProuct != curSymbol)
                return false;
        }

        //log.info("Это информационное сообщение!");
        return true;
    }

    // the method sets the price in $
    public void SetPrice()  {

        WebElement dropDown = driver.findElement(By.xpath(
                "//div[@class='currency-selector dropdown js-dropdown']"));
        dropDown.findElement(By.tagName("a")).click();

        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        String currency = this.property.getProperty("db.USD");
        String xpath = "//*/ul/li/a[@title=\""+currency+"\"]";

        dropDown.findElement(By.xpath(xpath)).click();
    }

    // product search method by word returns a new page
    public SearchResultPage Search(String str){
      WebElement inputSearch =  driver.findElement(By.xpath(
                "//div[@id='search_widget']/form/input[@type='text']"));
      inputSearch.sendKeys(str);
      inputSearch.sendKeys(Keys.ENTER);
      String newUrl = driver.getCurrentUrl();

        return new SearchResultPage(this.driver,newUrl);
    }
}
