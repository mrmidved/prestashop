package ua.prestashopPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ua.prestaShopBase.Base;
import ua.prestaShopBase.LogerEventHandler;
import ua.prestaShopModels.Price;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchResultPage extends Base {

    private  List<WebElement> products;
    private ArrayList <String> priceResult;

    public SearchResultPage(WebDriver driver,String url) {
        super(driver);
        this.priceResult = new ArrayList<String>();
        this.efwd.get(url);
    }

    private List<WebElement> GetProducts()
    {
        this.products = driver.findElements(By.xpath(
                "//div[@id='js-product-list']/div/article/div/div/div/span[@itemprop = 'price']"));
        return this.products;
    }
    private boolean IsSorted(ArrayList<Double> arr)
    {
        for(int i = 1; i < arr.size(); i++)
            if( arr.get(i - 1) < arr.get(i))
               return false;

        return  true;
    }
    private String[] GetPrice(String str)
    {
        return str.split("\n");
    }


    // the method checks the actual number of products on the page with the specified
    public boolean IsContainsData() {
        String countProduct = driver.findElement(By.xpath(
                "//div[@id='js-product-list-top']/div[1]/p"))
                .getText()
                .replaceAll("\\D+","");

        int labelCount = Integer.parseInt(countProduct,10);
        int productCount = this.GetProducts().size();

        return labelCount == productCount;
    }

    // the method checks whether the price is set to $ or not
    public boolean IsPriceInUSD() {
        int currencyForComparison =(int)this.property.getProperty("db.currencyForComparison")
                .charAt(0);
        int currencyInLabel;
        for(WebElement el:this.products){
            //System.out.println(el.getText());
            currencyInLabel =(int)el.getText().replaceAll("[\\d,\\s,.]","")
                    .charAt(0);
            if(currencyInLabel != currencyForComparison)
                return false;
        }
        return true;
    }

    // method sorts search results
    public void  SetSortingHighToLow() {
        WebElement dropDown = driver.findElement(By.cssSelector("" +
                "[class=\"col-sm-12 col-xs-12 col-md-9 products-sort-order dropdown\"]"));
        dropDown.findElement(By.tagName("a")).click();

        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        String xpath = "//*/div[@class='dropdown-menu']/a[contains(text(),'" + this.property.getProperty("db.sorting")+"')]";
        dropDown.findElement(By.xpath(xpath)).click();

    }

    // the method checks that the goods are sorted without discount
    public boolean IsCorrectSorting() {
        WebElement flag = driver.findElement(By.xpath("//div[@id='js-product-list']/div/article//div/a"));
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.invisibilityOf(flag));

        this.products.clear();
        this.products = driver.findElements(By.cssSelector("[class=\"product-price-and-shipping\"]"));

        for(WebElement el : this.products)
            this.priceResult.add(el.getText());

        ArrayList <Double> result = new ArrayList<Double>();
        for(String str : priceResult)
            result.add(Price.GetStringForDouble(this.GetPrice(str)[0]));

//        for(double p : result)
//            System.out.println(p);

        return this.IsSorted(result);
    }

    // the method checks that the price is present before and after the discount
    public boolean IsProductsPriceBeforeAfterDiscount() {
        for (String strPrice : this.priceResult )
        {
            if(strPrice.contains("%"))
            {
               if(this.GetPrice(strPrice).length !=3)
                    return false;
            }
        }
        return true;
    }

    // method checks correct discount calculation
    public boolean IsPercentageProduct() {
        Price price = new Price();
        String [] arr;
        for (String strPrice : this.priceResult )
        {
            if(strPrice.contains("%"))
            {
               arr = this.GetPrice(strPrice);
               if(arr.length==3)
               {
                   price.SetPrice(arr[0],arr[1],arr[2]);
                   if(!price.IsCorrectDiscount())
                      return  false;
               }
            }
        }
        return true;
    }

}
