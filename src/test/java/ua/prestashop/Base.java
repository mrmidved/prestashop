package ua.prestashop;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ua.prestaShopBase.Property;
import java.util.concurrent.TimeUnit;

public class Base extends Property {

    protected WebDriver driver;
    private String url;
    private String driverChromePath;

    public Base()
    {
        super();
        this.url = this.property.getProperty("db.Url");
        this.driverChromePath = this.property.getProperty("db.chromeDriver");

        System.setProperty("webdriver.chrome.driver",this.driverChromePath);
        this.driver = new ChromeDriver();
    }

    @Before
    public void Open() {
        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(this.url);
    }

    @After
    public void Close() {
        this.driver.quit();
    }
}
