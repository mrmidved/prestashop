package ua.prestashop;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import ua.prestashopPages.HomePage;
import ua.prestashopPages.SearchResultPage;

public class PrestaShopTestPage extends Base {

    private WebDriver driver;
    private HomePage homePage;
    private SearchResultPage resultPage;

    public PrestaShopTestPage()  {
        super();
        this.driver = super.driver;
        this.homePage = new HomePage(this.driver);
    }

    // HomePage
    @Test
    public void testIsCurrencyPriceCheck() {
        try{
            Assert.assertTrue(this.homePage.IsCurrencyPriceCheck());
            System.out.println("The currency in the header matches the currency specified in the product.");
        }
        catch (Exception e){
            System.out.println("--- The currency in the header Not matches the currency specified in the product.");
        }
    }

    // SearchResultPage
    @Test
    public void testIsContainsData() {
        // Price set in $
        try{
            this.homePage.SetPrice();
            System.out.println("Price set in $");
        }
        catch (Exception e){
            System.out.println("--- Price in USD not established");
        }

        // search products by word
        try{
            this.resultPage = homePage.Search(this.property.getProperty("db.str"));
            System.out.println("Items found");
        }
        catch (Exception e){
            System.out.println("--- Items Not found");
        }

        // Check the number of items found
        try{
            boolean isContains = this.resultPage.IsContainsData();
            Assert.assertTrue(isContains);
            System.out.println("Number of products after searching for matches");
        }
        catch (Exception e){
            System.out.println("--- The number of products after the search does not match");
        }

        // checking that the price was in $
        try{
            boolean isPriceInUSD = this.resultPage.IsPriceInUSD();
            Assert.assertTrue(isPriceInUSD);
            System.out.println("Сurrency of the item found $");
        }
        catch (Exception e){
            System.out.println("--- Сurrency of the item found is Not a $");
        }

        // setting the sorting of goods from high to low
        try{
            this.resultPage.SetSortingHighToLow();
            System.out.println("Sorted by");
        }
        catch (Exception e){
            System.out.println("--- Sort failed");
        }

        // validation sorting
        try{
            boolean isCorrectSorting = this.resultPage.IsCorrectSorting();
            Assert.assertTrue(isCorrectSorting);
            System.out.println("Product sorted correctly");
        }
        catch (Exception e){
            System.out.println("--- Item not sorted correctly");
        }

        // check that two prices if discount
        try{
            boolean isProductsPriceBeforeAfterDiscount= this.resultPage.IsProductsPriceBeforeAfterDiscount();
            Assert.assertTrue(isProductsPriceBeforeAfterDiscount);
            System.out.println("In the promotional product two prices are indicated, the full price and the price for a discount.");
        }
        catch (Exception e){
            System.out.println("--- There is no price in the promotional product.");
        }

        // checking correct discount calculation
        try{
            boolean isPercentageProduct  = this.resultPage.IsPercentageProduct();
            Assert.assertTrue(isPercentageProduct);
            System.out.println("In the promotional product discount is calculated correctly");
        }
        catch (Exception e){
            System.out.println("--- In the promotional product the discount is not calculated correctly.");
        }
    }
}
